import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopping_cart/controllers/cart_controller.dart';
import 'package:shopping_cart/controllers/shopping_controller.dart';

@immutable
class ShoppingPage extends StatelessWidget {
  ShoppingPage({Key key}) : super(key: key);

  // NOTE:
  // Get.put(...) is used for dependency injection so that the same controller
  // is used each time it is called.  Not needed if only used once.
  final shoppingController = Get.put(ShoppingController());
  final cartController = Get.put(CartController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.teal,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              //NOTE: Have GetX control the ListView.builder(...)
              child: GetX<ShoppingController>(builder: (controller) {
                return ListView.builder(
                  itemCount: controller.products.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ProductCard(
                      productController: controller,
                      cartController: cartController,
                      index: index,
                    );
                  },
                );
              }),
            ),
            GetX<CartController>(
              builder: (cartController) {
                return Text(
                    "Total amount: \$${cartController.totalPrice.toStringAsFixed(2)}",
                    style: TextStyle(
                      fontSize: 32,
                      color: Colors.white,
                    ));
              },
            ),
            SizedBox(height: 100),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {},
        icon: Icon(
          Icons.shopping_cart_outlined,
          color: Colors.black,
        ),
        label: GetX<CartController>(builder: (cartController) {
          return Text(
            "${cartController.count}",
            style: TextStyle(
              fontSize: 20,
              color: Colors.black,
            ),
          );
        }),
        backgroundColor: Colors.amber,
      ),
    );
  }
}

class ProductCard extends StatelessWidget {
  ProductCard({
    @required this.index,
    @required this.productController,
    @required this.cartController,
    Key key,
  }) : super(key: key);

  ShoppingController productController;
  CartController cartController;
  int index;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(12),
      child: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${productController.products[index].productName}",
                      style: TextStyle(fontSize: 24),
                    ),
                    Text(
                        "${productController.products[index].productDescription}"),
                  ],
                ),
                Text(
                  "${productController.products[index].price}",
                  style: TextStyle(fontSize: 24),
                ),
              ],
            ),
            RaisedButton(
              onPressed: () {
                cartController.addToCart(productController.products[index]);
              },
              color: Colors.blue,
              textColor: Colors.white,
              child: Text("Add to Cart"),
            ),
          ],
        ),
      ),
    );
  }
}
