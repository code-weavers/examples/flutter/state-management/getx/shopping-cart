import 'package:get/get.dart';
import 'package:shopping_cart/models/product.dart';

class ShoppingController extends GetxController {
  // NOTE:
  // "products" can be bond to the user interface
  // ".obs" means observable
  // with ".obs" the interface will update as the data updates
  // without it, it won't
  var products = List<Product>().obs;

  @override
  void onInit() {
    super.onInit();
    fetchProducts();
  }

  void fetchProducts() async {
    await Future.delayed(Duration(seconds: 1));
    var productResult = [
      Product(
        id: 1,
        price: 30,
        productName: "First Product",
        productImage: "some_image",
        productDescription: "Some description about the product.",
      ),
      Product(
        id: 2,
        price: 40,
        productName: "Second Product",
        productImage: "some_image",
        productDescription: "Some description about the product.",
      ),
      Product(
        id: 3,
        price: 49.50,
        productName: "Third Product",
        productImage: "some_image",
        productDescription: "Some description about the product.",
      ),
      Product(
        id: 4,
        price: 50,
        productName: "Fourth Product",
        productImage: "some_image",
        productDescription: "Some description about the product.",
      ),
    ];  

    products.assignAll(productResult);

  }


}
