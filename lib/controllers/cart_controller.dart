import 'package:get/get.dart';
import 'package:shopping_cart/models/product.dart';

class CartController extends GetxController {
  var cartItems = List<Product>().obs;
  // Won't allow a field so using a getter to create a property
  double get totalPrice =>
      cartItems.fold(0.0, (sum, currentItem) => sum + currentItem.price);
  int get count => cartItems.length;

  addToCart(Product product) {
    cartItems.add(product);
  }

  // double getTotalPrice() {
  //   return cartItems.fold(0.0, (sum, currentItem) => sum + currentItem.price);
  // }
}
